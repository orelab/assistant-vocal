
const int trigPin = 3;
const int echoPin = 2;



void setup() 
{
  pinMode(trigPin, OUTPUT); // Prepare the trigger pin
  pinMode(echoPin, INPUT); // Prepare the echo pin
  Serial.begin(9600);
}



void loop()
{
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  long distance = time2Distance( pulseIn(echoPin,HIGH) );
  
  if( distance < 100 )
  {
    Serial.println("detected !");
  }
  
  delay(1000);
}

long time2Distance(long rawReply)
{
  long inch = rawReply/74/2;
  long cm = inch*2.54;

  return cm;
}
