var express = require('express');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

const Readline = require('@serialport/parser-readline');
const parser = new Readline();

var SerialPort = require('serialport');
var port = new SerialPort('/dev/ttyUSB0', {baudRate:9600});

server.listen(3000);

app.use(express.static('public'));

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/public/index.html');
});

io.on('connection', function (socket){

	port.on('data', function(data){
		console.log('Detection !');
		// parser.on('data', console.log);
		socket.emit('news', { hello: 'world' });
	});
});






